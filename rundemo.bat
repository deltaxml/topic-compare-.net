echo off
cd %CD%
set path=%path%;%SystemRoot%\Microsoft.NET\Framework\v3.5
echo clearing previous output
IF EXIST results (
rmdir results /s/q
)
mkdir results
echo starting 1 of 5
..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita results/dita-markup.dita output-format=dita-markup preservation-mode=roundTrip whitespace-processing-mode=ignore
echo starting 2 of 5
..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita results/arbortext-tcs.dita output-format=arbortext-tcs preservation-mode=roundTrip whitespace-processing-mode=ignore
echo starting 3 of 5
..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita results/framemaker-tcs.dita output-format=framemaker-tcs preservation-mode=roundTrip whitespace-processing-mode=ignore
echo starting 4 of 5
..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita results/oxygen-tcs.dita output-format=oxygen-tcs preservation-mode=roundTrip whitespace-processing-mode=ignore
echo starting 5 of 5
..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita results/xmetal-tcs.dita output-format=xmetal-tcs preservation-mode=roundTrip whitespace-processing-mode=ignore
echo complete
pause
