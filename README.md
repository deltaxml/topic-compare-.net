# Topic Compare sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DITA-Compare-8_0_0_n/samples/sample-name`.*

---

## Summary

This sample compares two DITA topic files in1.dita and in2.dita. Please read the content of the DITA markup result (results/dita-markup.dita) as it is designed to illustrate several of our DITA comparison product's features. For more details see: [Topic Compare documentation](https://docs.deltaxml.com/dita-compare/latest/topic-sample-3801192.html).

## Using a Batch File

Run the rundemo.bat batch file either by entering rundemo from the command line or by double-clicking on this file from Windows Explorer. This script runs the same comparison 5 times, once for each output format. The output destination is a freshly created results directory.

## Running the sample from the Command line
The sample can be run directly by issuing the following command

    ..\..\bin\deltaxml-dita.exe compare topic in1.dita in2.dita out.dita output-format=dita-markup

Where `dita-markup` format enumeration can be replaced by one of the tracked changes format enumerations.